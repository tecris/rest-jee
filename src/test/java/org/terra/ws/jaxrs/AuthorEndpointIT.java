package org.terra.ws.jaxrs;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.junit.Test;
import org.terra.ws.jaxrs.model.Author;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.RestAssured;

public class AuthorEndpointIT {

    private static final String REST_URL = "http://localhost:8080/rest/authors";

    @Test
    public void test() throws JsonProcessingException {

        String firstName = "David";
        String lastName = "Mitchell";
        String about = "blue sky";
        RestAssured.baseURI = "http://localhost:8080/rest/authors";

        Author author = new Author();
        author.setAbout(about);
        author.setFirstname(firstName);
        author.setLastname(lastName);
        
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(author);

        author = given().contentType("application/json").body(jsonInString).when().post(REST_URL).as(Author.class);

        get(REST_URL + "/" + author.getId()).then().body("firstname", equalTo(firstName)).body("lastname",
                equalTo(lastName)).body("about", equalTo(about));
    }

}

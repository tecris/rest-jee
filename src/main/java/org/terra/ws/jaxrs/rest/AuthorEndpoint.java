package org.terra.ws.jaxrs.rest;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.terra.ws.jaxrs.model.Author;

/**
 * 
 */
@Stateless
@Path("/authors")
public class AuthorEndpoint {

    private static final Logger LOGGER = Logger.getLogger(AuthorEndpoint.class.getName());
    
    private static final Map<Integer,Author> authorMap = new HashMap<>();

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response create(Author entity) {
        LOGGER.info("Create athor: " + entity);
        entity.setId(authorMap.size());
        authorMap.put(entity.getId(), entity);
        return Response.ok(entity).build();
    }

    @DELETE
    @Path("/{id:[0-9][0-9]*}")
    public Response deleteById(@PathParam("id") Integer id) {
        LOGGER.info("Delete author by id: " + id);
        Author toDelete = authorMap.get(id);
        if(toDelete!=null) {
            this.authorMap.remove(toDelete);
            return Response.noContent().build();
        } else {
            return Response.status(Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("/{id:[0-9][0-9]*}")
    @Produces("application/json")
    public Response findById(@PathParam("id") Integer id) {
        LOGGER.info("Find by author id: " + id);
        Author author = authorMap.get(id);
        LOGGER.info("Found: " + author);
        if(author!=null) {
            return Response.ok(author).build();
        } else {
            return Response.status(Status.NOT_FOUND).build();
        }
    }

    @PUT
    @Path("/{id:[0-9][0-9]*}")
    @Consumes("application/json")
    public Response update(@PathParam("id") Integer id, Author entity) {
        if (entity == null) {
            return Response.status(Status.BAD_REQUEST).build();
        }
        if (!id.equals(entity.getId())) {
            return Response.status(Status.CONFLICT).entity(entity).build();
        }
        LOGGER.info("Update athor: " + entity);
        return Response.noContent().build();
    }
}

# RESTful Web Services with JAX-RS

Basic RESTful Web Services with JAX-RS 


## Prerequisites
 - This project depends on some docker images that can be built using following script:
 - `./prerequisites.sh`
 - Follow [instructions](https://github.com/tecris/docker/blob/v3.4/nexus/README.md) to add jboss repository (as proxy repository) to nexus

## Continuous Delivery
 * `$ mvn verify -Pcd`
Use `-Dmaven.buildNumber.doCheck=false` if project contains local changes
 * Steps performed
  * build artifact
  * build web(wildfly) docker image with latest artifact
  * start web(wildfly) container
  * run integration tests
  * stop and remove wildfly container

### Run application with docker
- Start wildfly container
 - `# docker run -d --name wildfly9 -p 8080:8080 -p 9990:9990 casa.docker/wildfly:9.0.2`
- Deploy with maven 
 - `$ mvn wildfly:deploy`
- Undeploy with maven 
 - `$ mvn wildfly:undeploy`

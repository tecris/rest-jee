#!/bin/bash

git clone --branch v3.4 https://github.com/tecris/docker.git
docker build --no-cache -t casa.docker/jdk:8 docker/jdk/8
docker build --no-cache -t casa.docker/wildfly:9.0.2 docker/wildfly/9
docker build --no-cache -t casa.docker/nexus::2.11.4-01 docker/nexus
# start maven nexus repository
docker run --name nexus --restart=always -d -p 172.17.0.1:8081:8081 -v /opt/docker_volumes/nexus-data:/sonatype-work casa.docker/nexus:2.11.4-01
